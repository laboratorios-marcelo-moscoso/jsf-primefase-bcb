package com.cognos.controller;

import java.io.Serializable;
import java.util.List;

import com.cognos.dao.AlumnoDao;
import com.cognos.entity.Alumno;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import lombok.Getter;
import lombok.Setter;

@Named
@RequestScoped
@Getter
@Setter
public class AlumnoBean implements Serializable {

    @Inject
    private AlumnoDao alumnoDao;

    private Alumno nuevoAlumno = new Alumno();
    private List<Alumno> alumnos;

    @PostConstruct
    public void postConstruct(){
        alumnos = alumnoDao.getAllAlumnos();
    }

    public void registro(){
        alumnoDao.registroAlumno(nuevoAlumno);
        alumnos = alumnoDao.getAllAlumnos();
    }

    public List<Alumno> getAllAlumnos(){
        alumnos = alumnoDao.getAllAlumnos();
        return alumnos;
    }

    public void limpiarAlumno() {
        nuevoAlumno = new Alumno();
    }

    public void actualizarAlumno() {
        alumnoDao.actualizarAlumno(nuevoAlumno);
        limpiarAlumno();
        actualizarListaAlumnos();
    }

    public void actualizarListaAlumnos() {
        alumnos = alumnoDao.getAllAlumnos();
    }

    public void obtenerAlumno(Long id) {
        nuevoAlumno = alumnoDao.obtenerAlumno(id);
    }

    public void eliminarAlumno(Long id) {
        alumnoDao.eliminarAlumno(id);
        actualizarListaAlumnos();
    }
    
}
