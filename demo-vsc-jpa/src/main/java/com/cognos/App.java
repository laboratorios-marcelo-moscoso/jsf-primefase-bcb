package com.cognos;

import com.cognos.entity.Alumno;
import com.cognos.entity.Curso;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class App {
    public static void main(String[] args) {

        Alumno alumno = new Alumno();
        alumno.setNombres("Alejandra");
        alumno.setApellidos("Flores");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(alumno);
        em.getTransaction().commit();
  

        System.out.println("Alumno saved successfully");

        Curso curso = new Curso();
        curso.setNombre("Python");
        curso.setCategoria("Desarrollo de Software");
        curso.setVersion("v.3.10");

        em.getTransaction().begin();
        em.persist(curso);
        em.getTransaction().commit();

        em.close();
        emf.close();

        System.out.println("Curso saved successfully");


    }
}
