package com.cognos.dao;

import java.util.ArrayList;
import java.util.List;

import com.cognos.entity.Alumno;
import com.cognos.util.EntityManagerProducer;

import jakarta.enterprise.context.ApplicationScoped;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

@ApplicationScoped
public class AlumnoDao {

    public void registroAlumno(Alumno alumno) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            em.persist(alumno);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
            EntityManagerProducer.closeEntityManagerFactory();
        }

    }

    public List<Alumno> getAllAlumnos() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        EntityManager em = emf.createEntityManager();

        List<Alumno> alumnos = new ArrayList<>();

        try {
            alumnos = em.createQuery("SELECT a FROM Alumno a", Alumno.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
            emf.close();
        }

        return alumnos;
    }

    public Alumno obtenerAlumno(Long id) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        EntityManager em = emf.createEntityManager();

        Alumno alumno = new Alumno();

        try {
            alumno = em.find(Alumno.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
            emf.close();
        }

        return alumno;
    }

    public void eliminarAlumno(Long id) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        EntityManager em = emf.createEntityManager();

        try {

            Alumno alumno = em.find(Alumno.class, id);
            if (alumno != null) {
                em.getTransaction().begin();
                em.remove(alumno);
                em.getTransaction().commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
            emf.close();
        }
    }

    public void actualizarAlumno(Alumno alumno) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        EntityManager em = emf.createEntityManager();

        try {

            em.getTransaction().begin();
            em.merge(alumno);
            em.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
            emf.close();
        }
    }

}
