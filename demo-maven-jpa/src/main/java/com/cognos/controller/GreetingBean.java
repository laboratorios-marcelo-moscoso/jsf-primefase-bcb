package com.cognos.controller;

import java.io.Serializable;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;

import lombok.Getter;
import lombok.Setter;

@Named
@RequestScoped
@Getter
@Setter
public class GreetingBean implements Serializable {
    private String name;
    private String message;

    public void greeting(){
        message = "Hello " + name;
    }
    
}
